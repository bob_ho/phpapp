<?php

$view = "SELECT * from users";

$rows = db_select($view);

?>
<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search entry" title="Search">
<table class="table" id="table">
    <thead>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Address</th>
	<th>City</th>
	<th>State</th>
	<th>Zip Code</th>
	<th>Email</th>
	<th>Status</th>
	<th>Actions</th>
      </tr>
    </thead>
    <tbody>
<?php
foreach ($rows as $value) {
	extract($value);
	echo "<tr>
        <td>".$firstname."</td>
        <td>".$lastname."</td>
        <td>".$address."</td>
        <td>".$city."</td>
        <td>".$state."</td>
	<td>".$zipcode."</td>
	<td>".$email."</td>
        <td>".$status."</td>";
	if($id == $getid) {
	echo "<td><a href='?id=".$id."#modify' data-toggle='modal'>Edit</a> | <a href='?id=".$id."&del=1#del' data-toggle='modal' data-target='#delete'>Delete</a></td>
      	</tr></a>";
	} else {
	echo "<td><a href='?id=".$id."#modify'>Edit</a> | <a href='?id=".$id."&del=1#del'>Delete</a></td>
      </tr></a>";
	}
}
      unset($firstname, $lastname, $address, $city, $state, $zipcode, $email, $status);
?>
    </tbody>
  </table>
<script>

function myFunction() {
  // Declare variables 
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("table");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    td1 = tr[i].getElementsByTagName("td")[1];
    td2 = tr[i].getElementsByTagName("td")[2];
    td3 = tr[i].getElementsByTagName("td")[3];
    td4 = tr[i].getElementsByTagName("td")[4];
    td5 = tr[i].getElementsByTagName("td")[5];
    td6 = tr[i].getElementsByTagName("td")[6];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      }
      else if (td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      }
      else if (td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      }
      else if (td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      }
      else if (td4.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      }
      else if (td5.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      }
      else if (td6.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}
</script>
