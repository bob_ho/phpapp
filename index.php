<?php
	include('connectdb.php');
	$link = db_connect();
	$getid = mysqli_real_escape_string($link,htmlspecialchars($_GET['id']));
	if ($getid) {
		include("edit.php");
	}
	include("add.php");
?>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Test Application</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<?php include('javascript.php'); ?>

	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<div class="container">
		<h1 class="text-center">Test Application</h1>
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/phpapp">Home</a>
		    </div>
		 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      		<ul class="nav navbar-nav">
		<li><a href="/phpapp#modify">Add</a></li>
		</ul>
		</div>
		  </div><!-- /.container-fluid -->
		</nav>

<!-- -->

  <!-- Modal -->
  <div class="modal fade" id="modify" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
	<?php
	if ($getid) {
		echo '<h4 class="modal-title">Edit Entry</h4>';
	} else {
		echo '<h4 class="modal-title">Add Entry</h4>';
	}
	?>
        </div>
        <div class="modal-body">
         
<!-- -->

<form method="post">

<div class="form-group container">

<label for="firstname">First Name:</label>
<input type="text" name="firstname" value="<?php echo $firstname; ?>" required /><br/>

<label for="lastname">Last Name: </label>
<input type="text" name="lastname" value="<?php echo $lastname; ?>" required /><br/>

<label for="address">Address: </label>
<input type="text" name="address" value="<?php echo $address; ?>" required /><br/>

<label for="city">City: </label>
<input type="text" name="city" value="<?php echo $city; ?>" required /><br/>

<label for="state">State: </label>
<select name="state" required>
	<option value="AL" <?php if($state == "AL") {echo "selected='selected'";}?>>AL</option>
	<option value="AK" <?php if($state == "AK") {echo "selected='selected'";}?>>AK</option>
	<option value="AZ" <?php if($state == "AZ") {echo "selected='selected'";}?>>AZ</option>
	<option value="AR" <?php if($state == "AR") {echo "selected='selected'";}?>>AR</option>
	<option value="CA" <?php if($state == "CA") {echo "selected='selected'";}?>>CA</option>
	<option value="CO" <?php if($state == "CO") {echo "selected='selected'";}?>>CO</option>
	<option value="CT" <?php if($state == "CT") {echo "selected='selected'";}?>>CT</option>
	<option value="DE" <?php if($state == "DE") {echo "selected='selected'";}?>>DE</option>
	<option value="DC" <?php if($state == "DC") {echo "selected='selected'";}?>>DC</option>
	<option value="FL" <?php if($state == "FL") {echo "selected='selected'";}?>>FL</option>
	<option value="GA" <?php if($state == "GA") {echo "selected='selected'";}?>>GA</option>
	<option value="HI" <?php if($state == "HI") {echo "selected='selected'";}?>>HI</option>
	<option value="ID" <?php if($state == "ID") {echo "selected='selected'";}?>>ID</option>
	<option value="IL" <?php if($state == "IL") {echo "selected='selected'";}?>>IL</option>
	<option value="IN" <?php if($state == "IN") {echo "selected='selected'";}?>>IN</option>
	<option value="IA" <?php if($state == "IA") {echo "selected='selected'";}?>>IA</option>
	<option value="KS" <?php if($state == "KS") {echo "selected='selected'";}?>>KS</option>
	<option value="KY" <?php if($state == "KY") {echo "selected='selected'";}?>>KY</option>
	<option value="LA" <?php if($state == "LA") {echo "selected='selected'";}?>>LA</option>
	<option value="ME" <?php if($state == "ME") {echo "selected='selected'";}?>>ME</option>
	<option value="MD" <?php if($state == "MD") {echo "selected='selected'";}?>>MD</option>
	<option value="MA" <?php if($state == "MA") {echo "selected='selected'";}?>>MA</option>
	<option value="MI" <?php if($state == "MI") {echo "selected='selected'";}?>>MI</option>
	<option value="MN" <?php if($state == "MN") {echo "selected='selected'";}?>>MN</option>
	<option value="MS" <?php if($state == "MS") {echo "selected='selected'";}?>>MS</option>
	<option value="MO" <?php if($state == "MO") {echo "selected='selected'";}?>>MO</option>
	<option value="MT" <?php if($state == "MT") {echo "selected='selected'";}?>>MT</option>
	<option value="NE" <?php if($state == "NE") {echo "selected='selected'";}?>>NE</option>
	<option value="NV" <?php if($state == "NV") {echo "selected='selected'";}?>>NV</option>
	<option value="NH" <?php if($state == "NH") {echo "selected='selected'";}?>>NH</option>
	<option value="NJ" <?php if($state == "NJ") {echo "selected='selected'";}?>>NJ</option>
	<option value="NM" <?php if($state == "NM") {echo "selected='selected'";}?>>NM</option>
	<option value="NY" <?php if($state == "NY") {echo "selected='selected'";}?>>NY</option>
	<option value="NC" <?php if($state == "NC") {echo "selected='selected'";}?>>NC</option>
	<option value="ND" <?php if($state == "ND") {echo "selected='selected'";}?>>ND</option>
	<option value="OH" <?php if($state == "OH") {echo "selected='selected'";}?>>OH</option>
	<option value="OK" <?php if($state == "OK") {echo "selected='selected'";}?>>OK</option>
	<option value="OR" <?php if($state == "OR") {echo "selected='selected'";}?>>OR</option>
	<option value="PA" <?php if($state == "PA") {echo "selected='selected'";}?>>PA</option>
	<option value="RI" <?php if($state == "RI") {echo "selected='selected'";}?>>RI</option>
	<option value="SC" <?php if($state == "SC") {echo "selected='selected'";}?>>SC</option>
	<option value="SD" <?php if($state == "SD") {echo "selected='selected'";}?>>SD</option>
	<option value="TN" <?php if($state == "TN") {echo "selected='selected'";}?>>TN</option>
	<option value="TX" <?php if($state == "TX") {echo "selected='selected'";}?>>TX</option>
	<option value="UT" <?php if($state == "UT") {echo "selected='selected'";}?>>UT</option>
	<option value="VT" <?php if($state == "VT") {echo "selected='selected'";}?>>VT</option>
	<option value="VA" <?php if($state == "VA") {echo "selected='selected'";}?>>VA</option>
	<option value="WA" <?php if($state == "WA") {echo "selected='selected'";}?>>WA</option>
	<option value="WV" <?php if($state == "WV") {echo "selected='selected'";}?>>WV</option>
	<option value="WI" <?php if($state == "WI") {echo "selected='selected'";}?>>WI</option>
	<option value="WY" <?php if($state == "WY") {echo "selected='selected'";}?>>WY</option>
</select>
<label for="zipcode">Zip Code: </label>
<input type="text" pattern="[0-9]{5}" name="zipcode" value="<?php echo $zipcode; ?>" required /><br/>

<label for="email">Email: </label>
<input type="email" name="email" value="<?php echo $email; ?>" required /><br/>

</div>

<!-- -->
	
        </div>
        <div class="modal-footer">
	<input type="submit" class="btn btn-default" name="submit" value="Submit">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
</form>
      </div>
      
    </div>
  </div>

<!-- -->

<!-- -->

  <!-- Modal -->
  <div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Entry</h4>
        </div>
        <div class="modal-body">
         
<!-- -->

<form method="post">

<div class="form-group container">

<h3>Are you sure you want to delete this entry?</h3>

</div>

<!-- -->
	
        </div>
        <div class="modal-footer">
	<input type="submit" class="btn btn-default" name="submit" value="Delete">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
</form>
      </div>
      
    </div>
  </div>

<!-- -->
		<?php
			include("table.php");
		?>

	</div>

<script>
$(document).ready(function() {

  if(window.location.href.indexOf('#modify') != -1) {
    $('#modify').modal('show');
  }

if(window.location.href.indexOf('#del') != -1) {
    $('#delete').modal('show');
  }

});
</script>
</body>
</html>
